package com.example.demo.service;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;


import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;


@Service
public class ReportService {

	@Autowired
	ReportRepository reportRepository;
	// レコード全件取得
	public List<Report> findAllReport() {
		return reportRepository.findAll(Sort.by(Sort.Direction.DESC, "updatedDate"));
//		return reportRepository.findAll();
	}

	// レコード追加
	public void saveReport(Report report) {
		reportRepository.save(report);
	}

	public void deleteReport(Integer id) {
        reportRepository.deleteById(id);
    }
	// レコード1件取得
	public Report findById(Integer id) {
		Report report = (Report)reportRepository.findById(id).orElse(null);
		return report;
	}

	public List<Report> findReport(String start, String end) throws ParseException {

	        SimpleDateFormat Time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            if(!StringUtils.isEmpty(start)) {
            	start += " 00:00:00";
        	} else {
        		start = "2021-07-01 00:00:00";
        	}
            if(!StringUtils.isEmpty(end)) {
            	end += " 23:59:59";
            } else {
            	Date date = new Date();
            	end = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
            }
			Date startDate = Time.parse(start);
			Date endDate  = Time.parse(end);

			return reportRepository.findBycreatedDateBetween(startDate, endDate);
	}



}