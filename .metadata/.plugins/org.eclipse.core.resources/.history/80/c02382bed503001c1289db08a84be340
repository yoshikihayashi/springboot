package com.example.demo.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class ForumController {

	@Autowired
	ReportService reportService;
	//コメント
	@Autowired
	CommentService commentService;

	@GetMapping
	//投稿内容表示画面
	public ModelAndView top() {
		ModelAndView mav = new ModelAndView();
		//投稿を全件取得
		List<Report> contentData = reportService.findAllReport();
		List<Comment> textData = commentService.findAllComment();
		// 画面遷移先を指定
		mav.setViewName("/top");
		// 投稿データオブジェクトを保管
		mav.addObject("contents", contentData);
		mav.addObject("texts", textData);
		return mav;
	}

	// 新規投稿画面
	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Report report = new Report();
		// 画面遷移先を指定
		mav.setViewName("/new");
		// 準備した空のentityを保管
		mav.addObject("formModel", report);
		return mav;
	}

	// 投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
		// 投稿をテーブルに格納
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}


	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {
		reportService.deleteReport(id);
		return new ModelAndView("redirect:/");
	}
	// 編集画面
	@GetMapping("/edit/{id}")
	public ModelAndView findById(@PathVariable Integer id) {
	ModelAndView mav = new ModelAndView();
	// 編集する投稿を取得
	Report report = reportService.findById(id);
	// 編集する投稿をセット
	mav.addObject("formModel", report);
	// 画面遷移先を指定
	mav.setViewName("/edit");
	return mav;
	}

	// 編集処理
	@PutMapping("/update/{id}")
	public ModelAndView updateContent (@PathVariable Integer id, @ModelAttribute("formModel") Report report) {
	// UrlParameterのidを更新するentityにセット
	report.setId(id);
	// 編集した投稿を更新
	reportService.saveReport(report);
	// rootへリダイレクト
	return new ModelAndView("redirect:/");
	}

	@GetMapping("/comment/{id}")
	public ModelAndView comment(@ModelAttribute("formModel") @PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Comment comment = new Comment();
		// 画面遷移先を指定
		mav.setViewName("/comment");
		// 準備した空のentityを保管
		mav.addObject("formModel", comment);
		mav.addObject("contentId", id);
		return mav;
	}


	@PostMapping("/addComment/{id}")
	public ModelAndView addComment(@ModelAttribute("formModel")Comment comment, @PathVariable Integer id) {
		comment.setReportId(id);
		// 投稿をテーブルに格納
		commentService.saveComment(comment);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// コメント編集画面
	@GetMapping("/commentEdit/{id}")
	public ModelAndView editComment(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// 編集するコメントを取得
		Comment comment = commentService.editComment(id);
		// 編集するコメントをセット
		mav.addObject("formModel", comment);
		// 画面遷移先を指定
		mav.setViewName("/commentEdit");
		return mav;
	}

	// コメント編集処理
	@PutMapping("/commentUpdate/{id}")
	public ModelAndView updateComment (@ModelAttribute("formModel")Comment comment, @PathVariable Integer id) {
		// UrlParameterのidを更新するentityにセット
		comment.setReportId(id);
		// 編集したコメントを更新
		commentService.saveComment(comment);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}
	//コメント削除処理
	@DeleteMapping("/commentDelete/{id}")
	public ModelAndView deleteComment(@PathVariable Integer id) {
		commentService.deleteComment(id);
		return new ModelAndView("redirect:/");
	}




}
